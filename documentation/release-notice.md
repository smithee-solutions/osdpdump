changes in 0.50

added more decoding of LED command

changes in 0.43

added do-transfer command

changes in 0.42

remove extra debug logging

changes in 0.41

fix "paired" vs. "default" in SCS 11
added 64bit card test and input test


changes in 0.40

added ISO 8601 timestamp conversion to convert-saleae

changes in 0.36

add scbk type (default/paired) to osdp_CHLNG output

changes in 0.35

fix errors decoding encrypted packets

changes in 0.34

more detection of invalid lines
updated saleae converter

changes in 0.31

reorg make file to build doc.  make package requirs pandoc, make build does not.

changes in 0.30

add support for summary descriptions of PSIA PKOC messages

changes in 0.25

fix detect ccrypt as dup
fix wrong scs length check on lstatr
added init secure channel command sample

changes in 0.23

detect retransmitted command

changes in 0.22

added max size text command
cmds now directly in etc

changes in 0.8.2
fix processing of max size data
added more command samples

release notice for osdpdump 0.1.1-1

top level make builds more completely, add package build

more or less proper git public repo headers

includes saleae converter

